﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;

namespace Negocio.Clases
{

    public class EmailSender
    {

        public string Para { get; set; }
        public string Asunto { get; set; }
        public string Mensaje { get; set; }
        public List<string> ParaVarios { get; set; }




        public void SendEmail()
        {
            MailMessage mmsg = new MailMessage();

            mmsg.To.Add(this.Para);
            mmsg.Subject = this.Asunto;
            mmsg.SubjectEncoding = Encoding.UTF8;
            mmsg.Body = this.Mensaje;
            mmsg.BodyEncoding = Encoding.UTF8;

            mmsg.IsBodyHtml = false;
            mmsg.From = new MailAddress("restauranteduocuc2021@gmail.com");

            SmtpClient cliente = new SmtpClient();

            cliente.Credentials = new NetworkCredential("restauranteduocuc2021@gmail.com", "chilelomasgrande");

            cliente.Port = 587;
            cliente.EnableSsl = true;
            cliente.Host = "smtp.gmail.com";

            try
            {
                cliente.Send(mmsg);
            }
            catch (Exception e)
            {

                throw e;
            }

        }

        public void SendEmailVariosDestinos()
        {
            MailMessage mmsg = new MailMessage();

            foreach (string email in this.ParaVarios)
            {
                mmsg.To.Add(email);
            }

            mmsg.Subject = this.Asunto;
            mmsg.SubjectEncoding = Encoding.UTF8;
            mmsg.Body = this.Mensaje;
            mmsg.BodyEncoding = Encoding.UTF8;

            mmsg.IsBodyHtml = false;
            mmsg.From = new MailAddress("restauranteduocuc2021@gmail.com");

            SmtpClient cliente = new SmtpClient();

            cliente.Credentials = new NetworkCredential("restauranteduocuc2021@gmail.com", "chilelomasgrande");

            cliente.Port = 587;
            cliente.EnableSsl = true;
            cliente.Host = "smtp.gmail.com";

            try
            {
                cliente.Send(mmsg);
            }
            catch (Exception e)
            {

                throw e;
            }

        }






    }
}
