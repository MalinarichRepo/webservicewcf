﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Cocina
{
    public class Comanda
    {
        public string ID { get; set; }
        public string Hora_Emision { get; set; }
        public int Estado { get; set; }
        public string MesaID { get; set; }
        public List<Plato> Platos { get; set; }
        public string Comentario { get; set; }

        public static string GetIdNewComanda()
        {
            OracleConnection conn = DataAccess.DataAccess.GetConnection();
            
            OracleCommand cmd1 = new OracleCommand();

            cmd1.CommandText =
                "select count(*)+1 from comanda";
            cmd1.Connection = conn;

            OracleDataReader reader1 = cmd1.ExecuteReader();

            List<Comanda> ListaComandas = new List<Comanda>();

            string newID = string.Empty;

            while (reader1.Read())
            {
                newID = reader1.GetString(0);
            }
            conn.Close();
            return newID;
        }
        public bool RegistrarComanda(string mesaid,string mensaje,List<Plato> platos,string comandaid)
        {
            OracleConnection conn = DataAccess.DataAccess.GetConnection();
            bool estado = false;
            OracleCommand insertcomanda = new OracleCommand();

            Admin.Mesa.OcuparMesa(mesaid);

            if (mensaje == string.Empty)
            {
                mensaje = "Sin Comentario";
            }
            insertcomanda.CommandText =
                "insert into COMANDA (comandaid, hora_emision, estado_entrega, mesa_mesaid, comentario) VALUES (:comandaid, sysdate-(3/24), 0,:mesaid,:comentario)";
            insertcomanda.Parameters.Add(":comandaid", OracleDbType.NVarchar2).Value = comandaid;
            insertcomanda.Parameters.Add(":mesaid", OracleDbType.NVarchar2).Value = mesaid;
            insertcomanda.Parameters.Add(":comentario", OracleDbType.NVarchar2).Value = mensaje;


            insertcomanda.Connection = conn;

            OracleDataReader reader1 = insertcomanda.ExecuteReader();

            while (reader1.Read())
            {
                
            }

            foreach (Plato plato in platos)
            {
                

                for (int i = 0; i < plato.Cantidad; i++)
                {
                    OracleCommand insertpedido = new OracleCommand();

                    insertpedido.CommandText =
                    "insert into pedido (pedidoid, producto, comanda_comandaid) VALUES (SEC_PEDIDO.nextval, :platoid, :comandaid)";
                    insertpedido.Parameters.Add(":platoid", OracleDbType.NVarchar2).Value = plato.ID;
                    insertpedido.Parameters.Add(":comandaid", OracleDbType.NVarchar2).Value = comandaid;

                    insertpedido.Connection = conn;

                    OracleDataReader reader2 = insertpedido.ExecuteReader();

                    while (reader2.Read())
                    {
                        
                    }

                }
                estado = true;
            }

            conn.Close();
            return estado;
        }
        public List<Comanda> GetAllFalseComandas()
        {
            OracleConnection conn = DataAccess.DataAccess.GetConnection();

            OracleCommand cmd1 = new OracleCommand();

            cmd1.CommandText =
                "select COMANDAID,HORA_EMISION,ESTADO_ENTREGA,MESA_MESAID,COMENTARIO from COMANDA WHERE estado_entrega=0";

            /*cmd.Parameters.Add(":userid", OracleDbType.NVarchar2).Value = this.userid;*/


            cmd1.Connection = conn;

            OracleDataReader reader1 = cmd1.ExecuteReader();

            List<Comanda> ListaComandas = new List<Comanda>();



            while (reader1.Read())
            {
                Comanda comanda = new Comanda();

                comanda.ID = reader1.GetString(0);
                comanda.Hora_Emision = reader1.GetDateTime(1).ToString("HH:mm");
                comanda.Estado = reader1.GetInt32(2);
                comanda.MesaID = reader1.GetString(3);
                comanda.Comentario = reader1.GetString(4); 
                comanda.Platos = new List<Plato>();

                ListaComandas.Add(comanda);
            }

            foreach (Comanda comanda in ListaComandas)
            {
                OracleCommand cmd2 = new OracleCommand();
                /*
                cmd2.CommandText =
                    "select PLATO.ID,PLATO.NOMBRE_PLATO,PLATO.DESCRIPCION,PLATO.PRECIO from PEDIDO join PLATO on PEDIDO.PRODUCTO = PLATO.ID where PEDIDO.COMANDA_COMANDAID = :comandaid";
                */

                cmd2.CommandText =
                    "select count(*),PLATO.ID,PLATO.NOMBRE_PLATO,PLATO.DESCRIPCION,PLATO.PRECIO from PEDIDO join PLATO on PEDIDO.PRODUCTO = PLATO.ID where PEDIDO.COMANDA_COMANDAID = :comandaid group by PLATO.ID,PLATO.ID,PLATO.NOMBRE_PLATO,PLATO.DESCRIPCION,PLATO.PRECIO";
                cmd2.Parameters.Add(":comandaid", OracleDbType.NVarchar2).Value = comanda.ID;

                cmd2.Connection = conn;

                OracleDataReader reader2 = cmd2.ExecuteReader();

                while (reader2.Read())
                {
                    Plato plato = new Plato();

                    plato.Cantidad = reader2.GetInt32(0);

                    plato.ID = reader2.GetString(1);
                    plato.Nombre_Plato = reader2.GetString(2);
                    plato.Descripcion_Plato = reader2.GetString(3);
                    plato.Precio = reader2.GetInt32(4);

                    comanda.Platos.Add(plato);
                }

            }

            conn.Close();
            return ListaComandas;
        }

        public static bool ComandaCheck(string comandaid)
        {
            OracleConnection conn = DataAccess.DataAccess.GetConnection();

            OracleCommand cmd1 = new OracleCommand();

            cmd1.CommandText =
                "select estado_entrega from comanda where COMANDAID=:comandaid";

            cmd1.Parameters.Add(":comandaid", OracleDbType.NVarchar2).Value = comandaid;
            cmd1.Connection = conn;

            OracleDataReader reader1 = cmd1.ExecuteReader();

            List<Comanda> ListaComandas = new List<Comanda>();

            string newID = string.Empty;
            bool respuesta=false;
            while (reader1.Read())
            {
                respuesta = reader1.GetBoolean(0);
            }
            conn.Close();
            return respuesta;
        }
        public Negocio.Admin.RegistroMensaje ActualizarComanda()
        {
            Negocio.Admin.RegistroMensaje RM = new Admin.RegistroMensaje();

            OracleConnection conn = DataAccess.DataAccess.GetConnection();

            try
            {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText =
                "update COMANDA set COMANDA.ESTADO_ENTREGA=1 where comandaid=:comandaid";
                cmd.Parameters.Add(":comandaid", OracleDbType.NVarchar2).Value = this.ID;

                cmd.Connection = conn;

                cmd.ExecuteNonQuery();

                RM.Estado = true;
                RM.Mensaje = "comanda actualizada con exito";
            }
            catch (Exception e)
            {

                RM.Estado = false;
                RM.Mensaje = "Error al actualizar la comanda";
            }

            conn.Close();
            return RM;
        }

        public static List<Plato> GetComandaByID(string comandaid)
        {
            OracleConnection conn = DataAccess.DataAccess.GetConnection();
            List<Plato> ListPlatos = new List<Plato>();

            OracleCommand cmd2 = new OracleCommand();

            cmd2.CommandText =
                "select count(*),PLATO.ID,PLATO.NOMBRE_PLATO,PLATO.DESCRIPCION,PLATO.PRECIO from PEDIDO join PLATO on PEDIDO.PRODUCTO = PLATO.ID where PEDIDO.COMANDA_COMANDAID = :comandaid group by PLATO.ID,PLATO.ID,PLATO.NOMBRE_PLATO,PLATO.DESCRIPCION,PLATO.PRECIO";
            cmd2.Parameters.Add(":comandaid", OracleDbType.NVarchar2).Value = comandaid;

            cmd2.Connection = conn;

            OracleDataReader reader2 = cmd2.ExecuteReader();

            while (reader2.Read())
            {
                Plato plato = new Plato();

                plato.Cantidad = reader2.GetInt32(0);

                plato.ID = reader2.GetString(1);
                plato.Nombre_Plato = reader2.GetString(2);
                plato.Descripcion_Plato = reader2.GetString(3);
                plato.Precio = reader2.GetInt32(4);

                ListPlatos.Add(plato);
            }

            conn.Close();
            return ListPlatos;
        }
    }
}
