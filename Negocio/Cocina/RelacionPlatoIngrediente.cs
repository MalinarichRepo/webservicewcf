﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Cocina
{
    public class RelacionPlatoIngrediente
    {
        public string PlatoID { get; set; }
        public string IngredienteID { get; set; }
        public string CantidadConsumo { get; set; }
        public string Descripcion { get; set; }
        public string Medida { get; set; }


        public Negocio.Admin.RegistroMensaje AgregarRPI()
        {
            Negocio.Admin.RegistroMensaje RM = new Admin.RegistroMensaje();

            OracleConnection conn = DataAccess.DataAccess.GetConnection();

            try
            {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText =
                "insert into RELATION_PLATO_INGREDIENTE (PLATO_ID,INGREDIENTE_ID,CANTIDADINGREDIENTEUSOGR) values (:platoid,:ingredienteid,:consumo)";
                cmd.Parameters.Add(":platoid", OracleDbType.NVarchar2).Value = this.PlatoID;
                cmd.Parameters.Add(":ingredienteid", OracleDbType.NVarchar2).Value = this.IngredienteID;
                cmd.Parameters.Add(":consumo", OracleDbType.NVarchar2).Value = this.CantidadConsumo;

                cmd.Connection = conn;

                cmd.ExecuteNonQuery();

                RM.Estado = true;
                RM.Mensaje = "Ingrediente Agregado con exito";
            }
            catch (Exception e)
            {

                RM.Estado = false;
                RM.Mensaje = "Producto ya agregado a este plato";
            }

            conn.Close();
            return RM;
        }

        public static List<RelacionPlatoIngrediente> GetAllIngredienteByPlatoID(string platoID)
        {
            List<RelacionPlatoIngrediente> List = new List<RelacionPlatoIngrediente>();

            OracleConnection conn = DataAccess.DataAccess.GetConnection();
            OracleCommand cmd = new OracleCommand();
            cmd.CommandText =
                "select RPI.ingrediente_id,I.DESCRIPCION,M.MEDIDA_DESC,RPI.CANTIDADINGREDIENTEUSOGR from RELATION_PLATO_INGREDIENTE RPI join ingrediente I on RPI.ingrediente_ID = I.ID join STOCK_PRODUCTO SP on I.ID = SP.PRODUCTOID join medida M on M.MEDIDAID = SP.MEDIDA_MEDIDAID where RPI.PLATO_ID = :id";
            cmd.Parameters.Add(":id", OracleDbType.NVarchar2).Value = platoID;

            cmd.Connection = conn;
            OracleDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                RelacionPlatoIngrediente i = new RelacionPlatoIngrediente();


                i.IngredienteID = reader.GetString(0);
                i.Descripcion = reader.GetString(1);
                i.Medida = reader.GetString(2);
                i.CantidadConsumo = reader.GetString(3);

                List.Add(i);
            }

            conn.Close();
            return List;
        }


        public Negocio.Admin.RegistroMensaje EliminarRPI()
        {
            Negocio.Admin.RegistroMensaje RM = new Admin.RegistroMensaje();

            OracleConnection conn = DataAccess.DataAccess.GetConnection();

            try
            {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText =
                "DELETE FROM RELATION_PLATO_INGREDIENTE WHERE  PLATO_ID = :platoid and INGREDIENTE_ID = :ingredienteid";
                cmd.Parameters.Add(":platoid", OracleDbType.NVarchar2).Value = this.PlatoID;
                cmd.Parameters.Add(":ingredienteid", OracleDbType.NVarchar2).Value = this.IngredienteID;

                cmd.Connection = conn;

                cmd.ExecuteNonQuery();

                RM.Estado = true;
                RM.Mensaje = "Ingrediente Eliminado con exito";
            }
            catch (Exception e)
            {

                RM.Estado = false;
                RM.Mensaje = "El producto no pudo ser eliminado";
            }

            conn.Close();
            return RM;
        }
    }
}
