﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Cocina
{
    public class Reserva
    {
        public string reservaid { get; set; }
        public string usuarioid { get; set; }
        public string fecha { get; set; }
        public string hora_entrada { get; set; }
        public string hora_salida { get; set; }
        public string mesaid { get; set; }
        public bool estado { get; set; }


        public static bool CrearReserva(string userid,string fecha,string mesaid,string hora_entrada,string hora_salida)
        {

            OracleConnection conn = DataAccess.DataAccess.GetConnection();
            OracleCommand cmd1 = new OracleCommand();
            cmd1.CommandText =
                "select * from RESERVA where fecha = TO_DATE(:fecha, 'yyyy/mm/dd') and mesaid = :mesaid and to_number(substr(:hora_entrada,1,2),99) between to_number(substr(hora_entrada,1,2),99) and to_number(substr(hora_salida,1,2),99)-0.1";
            cmd1.Parameters.Add(":fecha", OracleDbType.NVarchar2).Value = fecha;
            cmd1.Parameters.Add(":mesaid", OracleDbType.NVarchar2).Value = mesaid;
            cmd1.Parameters.Add(":hora_entrada", OracleDbType.NVarchar2).Value = hora_entrada;

            cmd1.Connection = conn;
            OracleDataReader reader = cmd1.ExecuteReader();

            bool r1 = false;
            bool r2 = false;

            while (reader.Read())
            {
                r1 = true;
                break;
            }

            OracleCommand cmd2 = new OracleCommand();

            cmd2.CommandText =
                "select * from RESERVA where fecha = TO_DATE(:fecha, 'yyyy/mm/dd') and mesaid = :mesaid and to_number(substr(HORA_ENTRADA,1,2),99) between to_number(substr(:hora_entrada,1,2),99) and to_number(substr(:hora_salida,1,2),99)-0.1";
            cmd2.Parameters.Add(":fecha", OracleDbType.NVarchar2).Value = fecha;
            cmd2.Parameters.Add(":mesaid", OracleDbType.NVarchar2).Value = mesaid;
            cmd2.Parameters.Add(":hora_entrada", OracleDbType.NVarchar2).Value = hora_entrada;
            cmd2.Parameters.Add(":hora_salida", OracleDbType.NVarchar2).Value = hora_salida;

            cmd2.Connection = conn;
            OracleDataReader reader2 = cmd2.ExecuteReader();

            while (reader2.Read())
            {
                r2 = true;
                break;
            }

            bool respuesta;

            if (r2 || r1)
            {
                respuesta = false;
            }
            else
            {
                OracleCommand cmd3 = new OracleCommand();

                cmd3.CommandText =
                    "insert into RESERVA(USUARIOID,fecha,hora_entrada,hora_salida,mesaid,estado) VALUES (:usuarioid, to_date(:fecha, 'yyyy/mm/dd'),'"+ hora_entrada + "','" + hora_salida + "','" + mesaid + "', 0)";
                cmd3.Parameters.Add(":usuarioid", OracleDbType.NVarchar2).Value = userid;
                cmd3.Parameters.Add(":fecha", OracleDbType.NVarchar2).Value = fecha;
                //cmd3.Parameters.Add(":mesaid", OracleDbType.NVarchar2).Value = mesaid;
                //cmd3.Parameters.Add(":hora_entrada", OracleDbType.NVarchar2).Value = hora_entrada;
                //cmd3.Parameters.Add(":hora_salida", OracleDbType.NVarchar2).Value = hora_salida;

                cmd3.Connection = conn;
                OracleDataReader reader3 = cmd3.ExecuteReader();

                while (reader3.Read())
                {
                }
                respuesta = true;
            }

            conn.Close();
            return respuesta;
        }
        public static List<Reserva> GetReservarByUserID(string usuarioid)
        {
            List<Reserva> ListReserva = new List<Reserva>();

            OracleConnection conn = DataAccess.DataAccess.GetConnection();
            OracleCommand cmd = new OracleCommand();
            cmd.CommandText =
                "select reservaid,usuarioid,fecha,hora_entrada,hora_salida,mesaid,estado from reserva where usuarioid=:usuarioid";
            cmd.Parameters.Add(":usuarioid", OracleDbType.NVarchar2).Value = usuarioid;

            cmd.Connection = conn;
            OracleDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Reserva reserva = new Reserva();

                reserva.reservaid = reader.GetString(0);
                reserva.usuarioid = reader.GetString(1);
                reserva.fecha = reader.GetDateTime(2).ToString("dd/MM/yyyy");
                reserva.hora_entrada = reader.GetString(3);
                reserva.hora_salida = reader.GetString(4);
                reserva.mesaid = reader.GetString(5);
                reserva.estado = reader.GetBoolean(6);

                ListReserva.Add(reserva);
            }
            conn.Close();
            return ListReserva;
        }
    }
}
