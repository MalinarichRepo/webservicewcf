﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Cocina
{
    public class Plato
    {
        public string ID { get; set; }
        public string Nombre_Plato { get; set; }
        public string Descripcion_Plato { get; set; }
        public int Precio { get; set; }
        public byte[] Foto { get; set; }
        /*DATO SOLO UTIL AL LLAMAR A LAS COMANDAS*/
        public int Cantidad { get; set; }


        public Negocio.Admin.RegistroMensaje RegistrarPlato()
        {
            Negocio.Admin.RegistroMensaje RM = new Admin.RegistroMensaje();

            OracleConnection conn = DataAccess.DataAccess.GetConnection();

            try
            {
                OracleCommand comando = new OracleCommand("REGISTRAR_PLATO", conn);
                comando.CommandType = System.Data.CommandType.StoredProcedure;

                comando.Parameters.Add("v_nombre", OracleDbType.NVarchar2).Value = this.Nombre_Plato;
                comando.Parameters.Add("v_descripcion", OracleDbType.NVarchar2).Value = this.Descripcion_Plato;
                comando.Parameters.Add("v_precio", OracleDbType.Int32).Value = this.Precio;
                comando.Parameters.Add("v_foto", OracleDbType.Blob).Value = this.Foto;

                comando.ExecuteNonQuery();

                RM.Estado = true;
                RM.Mensaje = "Plato registrado con exito";
            }
            catch (Exception e)
            {

                RM.Estado = false;
                RM.Mensaje = "se produjo un error al registrar el plato";
            }

            conn.Close();
            return RM;
        }

        public static List<Plato> GetAllPlatos()
        {
            List<Plato> ListPlatos = new List<Plato>();

            OracleConnection conn = DataAccess.DataAccess.GetConnection();
            OracleCommand cmd = new OracleCommand();
            cmd.CommandText =
                "select * from plato";

            cmd.Connection = conn;
            OracleDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Plato plato = new Plato();

                plato.ID = reader.GetString(0);
                plato.Nombre_Plato = reader.GetString(1);
                plato.Descripcion_Plato = reader.GetString(2);
                plato.Precio = reader.GetInt32(3);
                plato.Foto = (byte[])(reader.GetOracleBlob(4)).Value;

                ListPlatos.Add(plato);
            }

            conn.Close();
            return ListPlatos;
        }

        public Plato GetPlatoByID()
        {
            OracleConnection conn = DataAccess.DataAccess.GetConnection();
            OracleCommand cmd = new OracleCommand();
            cmd.CommandText =
                "select * from plato where ID=:id";
            cmd.Parameters.Add(":id", OracleDbType.NVarchar2).Value = this.ID;

            cmd.Connection = conn;
            OracleDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                this.ID = reader.GetString(0);
                this.Nombre_Plato = reader.GetString(1);
                this.Descripcion_Plato = reader.GetString(2);
                this.Precio = reader.GetInt32(3);
                this.Foto = (byte[])(reader.GetOracleBlob(4)).Value;
                break;
            }

            conn.Close();
            return this;
        }

        public Negocio.Admin.RegistroMensaje EliminarPlatoByID()
        {
            Negocio.Admin.RegistroMensaje RM = new Admin.RegistroMensaje();

            OracleConnection conn = DataAccess.DataAccess.GetConnection();

            try
            {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText =
                "DELETE FROM PLATO WHERE ID=:id";
                cmd.Parameters.Add(":id", OracleDbType.NVarchar2).Value = this.ID;

                cmd.Connection = conn;

                cmd.ExecuteNonQuery();

                RM.Estado = true;
                RM.Mensaje = "Plato eliminado con exito";
            }
            catch (Exception e)
            {

                RM.Estado = false;
                RM.Mensaje = "Elimine todos los ingredientes referenciados a este plato antes de eliminar";
            }

            conn.Close();
            return RM;
        }

        public Negocio.Admin.RegistroMensaje EditarPlatoByID()
        {
            Negocio.Admin.RegistroMensaje RM = new Admin.RegistroMensaje();

            OracleConnection conn = DataAccess.DataAccess.GetConnection();

            try
            {
                OracleCommand comando = new OracleCommand("EDITAR_PLATO", conn);
                comando.CommandType = System.Data.CommandType.StoredProcedure;

                comando.Parameters.Add("v_id", OracleDbType.NVarchar2).Value = this.ID;
                comando.Parameters.Add("v_nombre", OracleDbType.NVarchar2).Value = this.Nombre_Plato;
                comando.Parameters.Add("v_descripcion", OracleDbType.NVarchar2).Value = this.Descripcion_Plato;
                comando.Parameters.Add("v_precio", OracleDbType.Int32).Value = this.Precio;
                comando.Parameters.Add("v_foto", OracleDbType.Blob).Value = this.Foto;

                comando.ExecuteNonQuery();

                RM.Estado = true;
                RM.Mensaje = "Plato actualizado con exito";
            }
            catch (Exception e)
            {

                RM.Estado = false;
                RM.Mensaje = "se produjo un error al actualizado el plato";
            }

            conn.Close();
            return RM;
        }
    }


    
}
