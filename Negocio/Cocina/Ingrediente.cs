﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Cocina
{
    public class Ingrediente
    {
        public string IngredienteID { get; set; }
        public string Descripcion { get; set; }
        public string Medida { get; set; }


        public static List<Ingrediente> GetAllIngredientes()
        {
            List<Ingrediente> ListIngredientes = new List<Ingrediente>();

            OracleConnection conn = DataAccess.DataAccess.GetConnection();
            OracleCommand cmd = new OracleCommand();
            cmd.CommandText =
                "select SP.PRODUCTOID,SP.DESCRIPCION,M.MEDIDA_DESC from STOCK_PRODUCTO SP join MEDIDA M on SP.MEDIDA_MEDIDAID=M.MEDIDAID";

            cmd.Connection = conn;
            OracleDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Ingrediente ingrediente = new Ingrediente();

                ingrediente.IngredienteID = reader.GetString(0);
                ingrediente.Descripcion = reader.GetString(1);
                ingrediente.Medida = reader.GetString(2);

                ListIngredientes.Add(ingrediente);
            }
            conn.Close();
            return ListIngredientes;
        }
    }
}
