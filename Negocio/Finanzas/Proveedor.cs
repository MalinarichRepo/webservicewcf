﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Finanzas
{
    public class Proveedor
    {
        public string Rut { get; set; }
        public string Nombre { get; set; }
        public string Telefono1 { get; set; }
        public string Telefono2 { get; set; }
        public string Direccion { get; set; }


        public Negocio.Admin.RegistroMensaje RegistrarProveedor()
        {
            Negocio.Admin.RegistroMensaje RM = new Negocio.Admin.RegistroMensaje();

            OracleConnection conn = DataAccess.DataAccess.GetConnection();

            OracleCommand cmd = new OracleCommand();

            cmd.CommandText =
                "select rut from proveedor where rut = :rut";

            cmd.Parameters.Add(":rut", OracleDbType.NVarchar2).Value = this.Rut;


            cmd.Connection = conn;

            OracleDataReader reader = cmd.ExecuteReader();

            RM.Estado = true;
            RM.Mensaje = "Proveedor registrado exitosamente.";

            while (reader.Read())
            {

                RM.Estado = false;
                RM.Mensaje = "Rut ya registrado.";

                break;
            }


            if (RM.Estado)
            {
                OracleCommand comando = new OracleCommand("REGISTRAR_PROVEEDOR", conn);
                comando.CommandType = System.Data.CommandType.StoredProcedure;

                comando.Parameters.Add("v_rut", OracleDbType.NVarchar2).Value = this.Rut;
                comando.Parameters.Add("v_nombre", OracleDbType.NVarchar2).Value = this.Nombre;
                comando.Parameters.Add("v_telefono1", OracleDbType.NVarchar2).Value = this.Telefono1;
                comando.Parameters.Add("v_telefono2", OracleDbType.NVarchar2).Value = this.Telefono2;
                comando.Parameters.Add("v_direccion", OracleDbType.NVarchar2).Value = this.Direccion;

                comando.ExecuteNonQuery();
            }
            conn.Close();
            return RM;
        }

        public static List<Proveedor> GetAllProveedores()
        {
            List<Proveedor> ListProveedor = new List<Proveedor>();

            OracleConnection conn = DataAccess.DataAccess.GetConnection();

            OracleCommand cmd = new OracleCommand();

            cmd.CommandText =
                "select * from proveedor";
            cmd.Connection = conn;

            OracleDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                Proveedor proveedor = new Proveedor();

                proveedor.Rut = reader.GetString(0);
                proveedor.Nombre = reader.GetString(1);
                proveedor.Telefono1 = reader.GetString(2);
                proveedor.Telefono2 = reader.GetString(3);
                proveedor.Direccion = reader.GetString(4);

                ListProveedor.Add(proveedor);
            }

            conn.Close();
            return ListProveedor;
        }

    }
}
