﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Finanzas
{
    public class DetalleFactura
    {
        public string Idproducto { get; set; }
        public string Descripcion { get; set; }
        public string Medida_desc { get; set; }
        public string Cantidad { get; set; }


        public static List<DetalleFactura> GetDetallesByIDFactura(string idfactura)
        {
            List<DetalleFactura> listdetalle = new List<DetalleFactura>();

            OracleConnection conn = DataAccess.DataAccess.GetConnection();

            OracleCommand cmd = new OracleCommand();

            cmd.CommandText =
                "select PF.IDPRODUCTO,SP.DESCRIPCION,M.MEDIDA_DESC,PF.CANTIDAD from PRODUCTOS_FACTURA PF join STOCK_PRODUCTO SP on SP.productoid=PF.IDPRODUCTO join MEDIDA M on M.MEDIDAID=SP.MEDIDA_MEDIDAID where PF.IDFACTURA=:idfactura";

            cmd.Parameters.Add(":idfactura", OracleDbType.NVarchar2).Value = idfactura;

            cmd.Connection = conn;

            OracleDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                DetalleFactura DF = new DetalleFactura();

                DF.Idproducto = reader.GetString(0);
                DF.Descripcion = reader.GetString(1);
                DF.Medida_desc = reader.GetString(2);
                DF.Cantidad = reader.GetString(3);

                listdetalle.Add(DF);
            }

            conn.Close();
            return listdetalle;
        }



    }
}
