﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Finanzas
{
    public class Producto
    {
        public string IDSelected { get; set; }
        public string Cantidad { get; set; }

        public string Descripcion { get; set; }
        public string MedidaID { get; set; }


        public bool RegistrarProducto()
        {
            OracleConnection conn = DataAccess.DataAccess.GetConnection();
            bool estado;
            try
            {
                OracleCommand comando = new OracleCommand("REGISTRO_PRODUCTO", conn);
                comando.CommandType = System.Data.CommandType.StoredProcedure;

                comando.Parameters.Add("V_DESCRIPCION", OracleDbType.NVarchar2).Value = this.Descripcion;
                comando.Parameters.Add("V_MEDIDAID", OracleDbType.NVarchar2).Value = this.MedidaID;

                comando.ExecuteNonQuery();

                estado = true;
            }
            catch (Exception e)
            {

                estado = false;
                throw e;
            }


            conn.Close();
            return estado;
        }
    }
}
