﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Finanzas
{
    public class RecordUtilDiaria
    {
        public string ID { get; set; }
        public DateTime Fecha { get; set; }
        public decimal Gasto { get; set; }
        public decimal Ingreso { get; set; }
        public decimal Utilidad { get; set; }
        public decimal Clientes_Atendido { get; set; }
        public decimal Platos_Consumidos { get; set; }
        public decimal Media_Tiempo_Atencion_HR { get; set; }


        public static List<RecordUtilDiaria> GetAllRecords()
        {
            List<RecordUtilDiaria> ListRecord = new List<RecordUtilDiaria>();

            OracleConnection conn = DataAccess.DataAccess.GetConnection();

            OracleCommand cmd = new OracleCommand();

            cmd.CommandText =
                "select * from RECORD_UTILIDADDIARIA";

            cmd.Connection = conn;

            OracleDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                RecordUtilDiaria r = new RecordUtilDiaria();

                r.ID = reader.GetString(0);
                r.Fecha = reader.GetDateTime(1);
                r.Gasto = reader.GetDecimal(2);
                r.Ingreso = reader.GetDecimal(3);

                r.Utilidad = reader.GetDecimal(4);
                r.Clientes_Atendido = reader.GetDecimal(5);
                r.Platos_Consumidos = reader.GetDecimal(6);
                r.Media_Tiempo_Atencion_HR = reader.GetDecimal(7);

                ListRecord.Add(r);
            }

            conn.Close();

            return ListRecord;
        }



    }
}
