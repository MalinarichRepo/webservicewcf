﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Finanzas
{
    public class Medida
    {
        public string Medidaid { get; set; }
        public string Medida_desc { get; set; }


        public static List<Medida> GetAllMedidas()
        {
            List<Medida> ListMedidas = new List<Medida>();

            OracleConnection conn = DataAccess.DataAccess.GetConnection();

            OracleCommand cmd = new OracleCommand();

            cmd.CommandText =
                "select * from MEDIDA";
            cmd.Connection = conn;

            OracleDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                Medida medida = new Medida();

                medida.Medidaid = reader.GetString(0);
                medida.Medida_desc = reader.GetString(1);

                ListMedidas.Add(medida);
            }

            conn.Close();
            return ListMedidas;
        }

    }
}
