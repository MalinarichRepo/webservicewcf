﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Finanzas
{
    public class Factura
    {

        public string ID { get; set; }
        public string Fecha_Emision { get; set; }
        public string Tipo_Mercaderia { get; set; }
        public string Valor_Neto { get; set; }
        public string Valor_Bruto { get; set; }
        public string Rut_Proveedor { get; set; }
        public bool Estado { get; set; }
        public List<Producto> Pedido { get; set; }

        public bool RegistrarFactura()
        {
            OracleConnection conn = DataAccess.DataAccess.GetConnection();
            bool estado;
            try
            {
                OracleCommand comandoGetID = new OracleCommand();
                comandoGetID.CommandText =
                    "select count(*) from FACTURA";

                comandoGetID.Connection = conn;
                OracleDataReader reader = comandoGetID.ExecuteReader();
                while (reader.Read())
                {
                    this.ID = reader.GetString(0);
                    break;
                }



                OracleCommand comando = new OracleCommand("REGISTRO_FACTURA", conn);
                comando.CommandType = System.Data.CommandType.StoredProcedure;

                comando.Parameters.Add("V_ID", OracleDbType.NVarchar2).Value = this.ID;
                comando.Parameters.Add("V_FECHA_EMISION", OracleDbType.NVarchar2).Value = this.Fecha_Emision;
                comando.Parameters.Add("V_TIPO_MERCADERIA", OracleDbType.NVarchar2).Value = this.Tipo_Mercaderia;
                comando.Parameters.Add("V_VALOR_NETO", OracleDbType.Int32).Value = this.Valor_Neto;
                comando.Parameters.Add("V_VALOR_BRUTO", OracleDbType.Int32).Value = this.Valor_Bruto;
                comando.Parameters.Add("V_RUT_PROVEEDOR", OracleDbType.NVarchar2).Value = this.Rut_Proveedor;

                comando.ExecuteNonQuery();

                

                foreach (Producto p in this.Pedido)
                {
                    OracleCommand comandoRegistrarProductos = new OracleCommand();
                    comandoRegistrarProductos.CommandText =
                        "insert into PRODUCTOS_FACTURA (IDFACTURA,IDPRODUCTO,CANTIDAD) VALUES (:v_id,:v_idproducto,:v_cantidad)";
                    comandoRegistrarProductos.Parameters.Add(":v_id", OracleDbType.NVarchar2).Value = this.ID;
                    comandoRegistrarProductos.Parameters.Add(":v_idproducto", OracleDbType.NVarchar2).Value = p.IDSelected;
                    comandoRegistrarProductos.Parameters.Add(":v_cantidad", OracleDbType.NVarchar2).Value = p.Cantidad;

                    comandoRegistrarProductos.Connection = conn;
                    OracleDataReader readerproductos = comandoRegistrarProductos.ExecuteReader();
                    while (readerproductos.Read())
                    {
                        
                    }
                }



                estado = true;
            }
            catch (Exception e)
            {

                estado = false;
            }

            conn.Close();
            return estado;
        }


        public static List<Factura> GetAllFacturas()
        {
            List<Factura> listfacturas = new List<Factura>();

            OracleConnection conn = DataAccess.DataAccess.GetConnection();
            OracleCommand cmd = new OracleCommand();
            cmd.CommandText =
                "select * from factura";

            cmd.Connection = conn;
            OracleDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Factura factura = new Factura();

                factura.ID = reader.GetString(0);
                factura.Fecha_Emision = reader.GetString(1);
                factura.Tipo_Mercaderia = reader.GetString(2);
                factura.Valor_Neto = reader.GetString(3);
                factura.Valor_Bruto = reader.GetString(4);
                factura.Rut_Proveedor = reader.GetString(5);
                factura.Estado = reader.GetBoolean(6);
                listfacturas.Add(factura);
            }

            conn.Close();
            return listfacturas;
        }

        public bool ActualizarFactura()
        {
            OracleConnection conn = DataAccess.DataAccess.GetConnection();
            bool estado;
            try
            {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText =
                "update FACTURA set ESTADO=1 where factura_id=:facturaid";
                cmd.Parameters.Add(":facturaid", OracleDbType.NVarchar2).Value = this.ID;

                cmd.Connection = conn;

                cmd.ExecuteNonQuery();
                estado = true;
            }
            catch (Exception e)
            {

                estado = false;
            }

            conn.Close();
            return estado;
        }
    }
}
