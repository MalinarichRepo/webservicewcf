﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Bodega
{
    public class Stock
    {
        public string id { get; set; }
        public string descripcion { get; set; }
        public int cantidad { get; set; }
        public string medida { get; set; }


        public static List<Stock> GetAllStock()
        {
            List<Stock> ListStock = new List<Stock>();

            OracleConnection conn = DataAccess.DataAccess.GetConnection();

            OracleCommand cmd1 = new OracleCommand();

            cmd1.CommandText =
                "select STOCK_PRODUCTO.PRODUCTOID,STOCK_PRODUCTO.DESCRIPCION,STOCK_PRODUCTO.CANTIDAD,MEDIDA.MEDIDA_DESC from STOCK_PRODUCTO join MEDIDA on MEDIDA.MEDIDAID=STOCK_PRODUCTO.MEDIDA_MEDIDAID";


            cmd1.Connection = conn;

            OracleDataReader reader1 = cmd1.ExecuteReader();

            while (reader1.Read())
            {
                Stock stock = new Stock();

                stock.id = reader1.GetString(0);
                stock.descripcion = reader1.GetString(1);
                stock.cantidad = reader1.GetInt32(2);
                stock.medida= reader1.GetString(3);

                ListStock.Add(stock);
            }

            conn.Close();
            return ListStock;
        }

        public bool PedirInsumo(string mensaje)
        {
            bool respuesta =false;

            OracleConnection conn = DataAccess.DataAccess.GetConnection();

            OracleCommand cmd1 = new OracleCommand();

            cmd1.CommandText =
                "select usuario.email from usuario join RELATION_USUARIO_ROL on usuario.userid=RELATION_USUARIO_ROL.usuario_userid where rol_rolid=0";

            cmd1.Connection = conn;

            OracleDataReader reader1 = cmd1.ExecuteReader();

            List<string> ListString = new List<string>();

            while (reader1.Read())
            {
                ListString.Add(reader1.GetString(0));
            }

            Clases.EmailSender ES = new Clases.EmailSender();

            ES.ParaVarios = ListString;
            ES.Asunto = "Pedido de insumo";
            ES.Mensaje = mensaje;

            ES.SendEmailVariosDestinos();
            conn.Close();
            respuesta = true;
            return respuesta;
        }

    }
}
