﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System.Configuration;
using System.Data;

namespace Negocio.Admin
{
    public class Rol
    {
        public string RolID { get; set; }
        public string Descripcion { get; set; }


        public static List<Rol> GetRols()
        {
            List<Rol> lr = new List<Rol>();

            OracleConnection conn = DataAccess.DataAccess.GetConnection();

            OracleCommand cmd = new OracleCommand();

            cmd.CommandText =
                "select ROLID, ROLDESCRIPCION from ROL";

            cmd.Connection = conn;

            OracleDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                Rol r = new Rol();

                r.RolID = reader.GetString(0);
                r.Descripcion = reader.GetString(1);

                lr.Add(r);                
            }

            conn.Close();
            return lr;
        }

    }
}
