﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System.Configuration;
using System.Data;

namespace Negocio.Admin
{
    public class Usuario
    {
        public string Rut { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Email { get; set; }
        public string Contrasenia { get; set; }
        public string rol { get; set; }
        public string userid { get; set; }
        public string roldescripcion { get; set; }
        public string Fecha { get; set; }

        public RegistroMensaje Registrar()
        {
            RegistroMensaje RM = new RegistroMensaje();

            OracleConnection conn = DataAccess.DataAccess.GetConnection();

            OracleCommand cmd = new OracleCommand();

            cmd.CommandText =
                "select email from usuario where email = :email";

            cmd.Parameters.Add(":email", OracleDbType.NVarchar2).Value = this.Email;
            

            cmd.Connection = conn;

            OracleDataReader reader = cmd.ExecuteReader();

            RM.Estado = true;
            RM.Mensaje = "Usuario registrado exitosamente.";

            while (reader.Read())
            {

                RM.Estado = false;
                RM.Mensaje = "Email ya registrado.";

                break;
            }


            if (RM.Estado)
            {
                OracleCommand comando = new OracleCommand("registro",conn);
                comando.CommandType = System.Data.CommandType.StoredProcedure;

                comando.Parameters.Add("v_rut", OracleDbType.NVarchar2).Value = this.Rut;
                comando.Parameters.Add("v_nombre", OracleDbType.NVarchar2).Value = this.Nombre;
                comando.Parameters.Add("v_apellido", OracleDbType.NVarchar2).Value = this.Apellido;
                comando.Parameters.Add("v_email", OracleDbType.NVarchar2).Value = this.Email;
                comando.Parameters.Add("v_contrasenia", OracleDbType.NVarchar2).Value = this.Contrasenia;
                comando.Parameters.Add("v_rol", OracleDbType.NVarchar2).Value = this.rol;

                comando.ExecuteNonQuery();

                Clases.EmailSender emailsender = new Clases.EmailSender();
                emailsender.Para = this.Email;
                emailsender.Asunto = "Registro Restaurante";
                emailsender.Mensaje = "Gracias por registrarte en nuestro sistema.";

                emailsender.SendEmail();
            }


            conn.Close();
            return RM;
        }

        public Usuario GetPerfil()
        {
            
            OracleConnection conn = DataAccess.DataAccess.GetConnection();

            OracleCommand cmd = new OracleCommand();

            cmd.CommandText =
                "select rut,nombre,apellido,email,rol.ROLDESCRIPCION,to_char(fecha_registro,'DD/MM/YYYY') from usuario join RELATION_USUARIO_ROL rur on usuario.userid=rur.USUARIO_USERID join rol on rur.rol_rolid=rol.rolid where usuario.USERID=:userid";

            cmd.Parameters.Add(":userid", OracleDbType.NVarchar2).Value = this.userid;


            cmd.Connection = conn;

            OracleDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {

                this.Rut = reader.GetString(0);
                this.Nombre = reader.GetString(1);
                this.Apellido = reader.GetString(2);
                this.Email = reader.GetString(3);
                this.roldescripcion = reader.GetString(4);
                this.Fecha = reader.GetString(5);

                break;
            }

            conn.Close();
            return this;
        }

        public Clases.Mensaje ResetPassword()
        {
            Clases.Mensaje msg = new Clases.Mensaje();

            OracleConnection conn = DataAccess.DataAccess.GetConnection();

            OracleCommand cmd = new OracleCommand();

            cmd.CommandText =
                "select email from usuario where email = :email";

            cmd.Parameters.Add(":email", OracleDbType.NVarchar2).Value = this.Email;


            cmd.Connection = conn;

            OracleDataReader reader = cmd.ExecuteReader();

            msg.Estado = false;
            msg.Message = "Correo electronico no registrado.";

            while (reader.Read())
            {

                msg.Estado = true;
                msg.Message = "Contraseña enviada al correo electronico.";

                break;
            }

            if (msg.Estado)
            {

                string newpassword = Clases.StringGenerator.Generate();

                OracleCommand comando = new OracleCommand("resetpassword", conn);
                comando.CommandType = System.Data.CommandType.StoredProcedure;

                comando.Parameters.Add("v_email", OracleDbType.NVarchar2).Value = this.Email;
                comando.Parameters.Add("v_newpassword", OracleDbType.NVarchar2).Value = newpassword;                

                comando.ExecuteNonQuery();

                Clases.EmailSender ES = new Clases.EmailSender();

                ES.Para = this.Email;
                ES.Asunto = "Recuperacion de contraseña";
                ES.Mensaje = "Su nueva contraseña es " + newpassword;

                ES.SendEmail();
            }

            conn.Close();
            return msg;
        }

        public Clases.Mensaje ChangePassword(string NewPassoword)
        {
            Clases.Mensaje msg = new Clases.Mensaje();

            OracleConnection conn = DataAccess.DataAccess.GetConnection();

            OracleCommand cmd = new OracleCommand();

            cmd.CommandText =
                "select email from usuario where userid = :userid";

            cmd.Parameters.Add(":userid", OracleDbType.NVarchar2).Value = this.userid;

            cmd.Connection = conn;

            OracleDataReader reader = cmd.ExecuteReader();

            msg.Estado = false;
            msg.Message = "Contraseña incorrecta.";

            while (reader.Read())
            {

                msg.Estado = true;
                msg.Message = "Contraseña cambiada con exito.";

                break;
            }

            if (msg.Estado)
            {

                string newpassword = NewPassoword;

                OracleCommand comando = new OracleCommand("resetpassword", conn);
                comando.CommandType = System.Data.CommandType.StoredProcedure;

                comando.Parameters.Add("v_email", OracleDbType.NVarchar2).Value = this.Email;
                comando.Parameters.Add("v_newpassword", OracleDbType.NVarchar2).Value = newpassword;

                comando.ExecuteNonQuery();

                Clases.EmailSender ES = new Clases.EmailSender();

                ES.Para = this.Email;
                ES.Asunto = "Cambio de contraseña";
                ES.Mensaje = "Le informamos que su cambio de contraseña se ha realizado con exito";

                ES.SendEmail();
            }

            conn.Close();
            return msg;
        }

        public Clases.Mensaje EditUser()
        {
            Clases.Mensaje msg = new Clases.Mensaje();

            OracleConnection conn = DataAccess.DataAccess.GetConnection();
            try
            {
                

                OracleCommand comando = new OracleCommand("EDITUSER", conn);
                comando.CommandType = System.Data.CommandType.StoredProcedure;

                comando.Parameters.Add("v_rut", OracleDbType.NVarchar2).Value = this.Rut;
                comando.Parameters.Add("v_nombre", OracleDbType.NVarchar2).Value = this.Nombre;
                comando.Parameters.Add("v_apellido", OracleDbType.NVarchar2).Value = this.Apellido;
                comando.Parameters.Add("v_email", OracleDbType.NVarchar2).Value = this.Email;

                comando.ExecuteNonQuery();
                

                Clases.EmailSender emailsender = new Clases.EmailSender();
                emailsender.Para = this.Email;
                emailsender.Asunto = "Usuario editado";
                emailsender.Mensaje = "Sus datos de usuario han sido editados correctamente";

                emailsender.SendEmail();

                msg.Estado = true;
                msg.Message = "Usuario editado con exito";

            }
            catch (Exception)
            {

                msg.Estado = false;
                msg.Message = "El usuario no pudo ser editado";
            }


            conn.Close();
            return msg;
        }

        public static List<Usuario> GetAllUsers()
        {
            List<Usuario> listusers = new List<Usuario>();

            OracleConnection conn = DataAccess.DataAccess.GetConnection();

            OracleCommand cmd = new OracleCommand();

            cmd.CommandText =
                "select USUARIO.USERID,USUARIO.RUT,USUARIO.NOMBRE,USUARIO.APELLIDO,to_char(USUARIO.FECHA_REGISTRO,'DD/MM/YYYY'),USUARIO.EMAIL,ROL.ROLDESCRIPCION from USUARIO join RELATION_USUARIO_ROL on usuario.userid = RELATION_USUARIO_ROL.USUARIO_USERID join ROL on ROL.ROLID = RELATION_USUARIO_ROL.ROL_ROLID";




            cmd.Connection = conn;

            OracleDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                Usuario newuser = new Usuario();

                newuser.userid = reader.GetString(0);
                newuser.Rut = reader.GetString(1);
                newuser.Nombre = reader.GetString(2);
                newuser.Apellido = reader.GetString(3);
                newuser.Fecha = reader.GetString(4);
                newuser.Email = reader.GetString(5);
                newuser.roldescripcion = reader.GetString(6);

                listusers.Add(newuser);
            }

            conn.Close();
            return listusers;
        }

        public Clases.Mensaje EliminarUsuario()
        {
            Clases.Mensaje msg = new Clases.Mensaje();

            OracleConnection conn = DataAccess.DataAccess.GetConnection();
            try
            {


                OracleCommand comando = new OracleCommand("ELIMINAR_USUARIO", conn);
                comando.CommandType = System.Data.CommandType.StoredProcedure;

                comando.Parameters.Add("v_rut", OracleDbType.NVarchar2).Value = this.userid;

                comando.ExecuteNonQuery();

                msg.Estado = true;
                msg.Message = "Usuario eliminado con exito";

            }
            catch (Exception)
            {

                msg.Estado = false;
                msg.Message = "El usuario no pudo ser Eliminado";
            }


            conn.Close();
            return msg;
        }
    }
}
