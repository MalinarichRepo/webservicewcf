﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Admin
{
    public class Mesa
    {
        public string Mesaid { get; set; }
        public bool Estado { get; set; }


        public static bool OcuparMesa(string mesaid)
        {
            OracleConnection conn = DataAccess.DataAccess.GetConnection();
            bool estado = false;
            try
            {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText =
                "update mesa set estado_uso=1 where mesaid=:mesaid";
                cmd.Parameters.Add(":mesaid", OracleDbType.NVarchar2).Value = mesaid;

                cmd.Connection = conn;

                cmd.ExecuteNonQuery();
                estado = true;
            }
            catch (Exception e)
            {

                
            }

            conn.Close();
            return estado;
        }

        public static bool DesocuparMesa(string mesaid)
        {
            OracleConnection conn = DataAccess.DataAccess.GetConnection();
            bool estado = false;
            try
            {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText =
                "update mesa set estado_uso=0 where mesaid=:mesaid";
                cmd.Parameters.Add(":mesaid", OracleDbType.NVarchar2).Value = mesaid;

                cmd.Connection = conn;

                cmd.ExecuteNonQuery();
                estado = true;
            }
            catch (Exception e)
            {


            }

            conn.Close();
            return estado;
        }



        public static List<Mesa> GetAllMesas()
        {
            List<Mesa> ListMesas = new List<Mesa>();

            OracleConnection conn = DataAccess.DataAccess.GetConnection();

            OracleCommand cmd = new OracleCommand();

            cmd.CommandText =
                "select * from mesa";
            cmd.Connection = conn;

            OracleDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                Mesa newmesa = new Mesa();

                newmesa.Mesaid = reader.GetString(0);
                newmesa.Estado = reader.GetBoolean(1);

                ListMesas.Add(newmesa);
            }

            conn.Close();
            return ListMesas;
        }

        public static Clases.Mensaje AgregarMesa()
        {
            Clases.Mensaje msg = new Clases.Mensaje();

            OracleConnection conn = DataAccess.DataAccess.GetConnection();
            try
            {


                OracleCommand comando = new OracleCommand("AGREGAR_MESA", conn);
                comando.CommandType = System.Data.CommandType.StoredProcedure;

                comando.ExecuteNonQuery();

                msg.Estado = true;
                msg.Message = "Mesa agregada con exito";

            }
            catch (Exception)
            {

                msg.Estado = false;
                msg.Message = "La mesa no pudo ser agregada";
            }


            conn.Close();
            return msg;
        }

        public static Clases.Mensaje EliminarMesa()
        {
            Clases.Mensaje msg = new Clases.Mensaje();

            OracleConnection conn = DataAccess.DataAccess.GetConnection();
            try
            {


                OracleCommand comando = new OracleCommand("ELIMINAR_MESA", conn);
                comando.CommandType = System.Data.CommandType.StoredProcedure;

                comando.ExecuteNonQuery();

                msg.Estado = true;
                msg.Message = "Mesa eliminada con exito";

            }
            catch (Exception)
            {

                msg.Estado = false;
                msg.Message = "La mesa no pudo ser eliminada";
            }


            conn.Close();
            return msg;
        }

    }
}
