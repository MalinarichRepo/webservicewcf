﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System.Configuration;
using System.Data;

namespace Negocio.Login
{
    public class Login
    {
        public static Clases.LoginRespuesta Loguear(Admin.Usuario user)
        {
            OracleConnection conn = DataAccess.DataAccess.GetConnection();

            OracleCommand cmd = new OracleCommand();

            cmd.CommandText =
                "select usuario.userid, relation_usuario_rol.rol_rolid from usuario join relation_usuario_rol on relation_usuario_rol.usuario_userid=usuario.userid where usuario.email like :useremail and usuario.contrasenia like :contrasenia";

            cmd.Parameters.Add(":useremail", OracleDbType.NVarchar2).Value = user.Email;
            cmd.Parameters.Add(":usercontrasenia", OracleDbType.NVarchar2).Value = user.Contrasenia;

            cmd.Connection = conn;

            

            OracleDataReader reader = cmd.ExecuteReader();

            Clases.LoginRespuesta LR = new Clases.LoginRespuesta();
            LR.Mensaje = "Login Error";

            while (reader.Read())
            {

                LR.Estado = true;
                LR.Mensaje = "Login Exitoso";                
                LR.UserID = reader.GetString(0);
                LR.RolID = reader.GetString(1);

                break;
            }


            conn.Close();
            return LR;
        }






    }
}
