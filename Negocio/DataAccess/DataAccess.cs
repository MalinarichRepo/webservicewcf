﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System.Configuration;
using System.Data;

namespace Negocio.DataAccess
{
    class DataAccess
    {
        
        public static OracleConnection GetConnection()
        {
            

            string ConnectionString = ConfigurationManager.ConnectionStrings["OracleDBcloud"].ConnectionString;

            OracleConnection conn = new OracleConnection(ConnectionString);

            try
            {
                conn.Open();
            }
            catch (Exception e)
            {
                throw e;
            }

            return conn;
        }
    }
}
