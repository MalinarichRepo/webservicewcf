﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ApiWCF.Admin
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IAdministrador" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IAdministrador
    {
        [OperationContract]
        string GetRols();
        [OperationContract]
        string Registrar(string usuario);
        [OperationContract]
        string GetAllUsers();
        [OperationContract]
        string GetAllMesas();
        [OperationContract]
        string EliminarUsuario(string id);
        [OperationContract]
        string AgregarMesa();
        [OperationContract]
        string EliminarMesa();

    }
    [DataContract]
    public class Rol
    {
        [DataMember]
        public string RolID { get; set; }
        [DataMember]
        public string Descripcion { get; set; }

    }
    [DataContract]
    public class RegistroMensaje
    {
        [DataMember]
        public bool Estado { get; set; }
        [DataMember]
        public string Mensaje { get; set; }

    }
    [DataContract]
    public class Usuario
    {
        [DataMember]
        public string Rut { get; set; }
        [DataMember]
        public string Nombre { get; set; }
        [DataMember]
        public string Apellido { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Contrasenia { get; set; }
        [DataMember]
        public string rol { get; set; }
        [DataMember]
        public string userid { get; set; }
        [DataMember]
        public string roldescripcion { get; set; }
        [DataMember]
        public string Fecha { get; set; }

    }
    [DataContract]
    public class Mesa
    {
        [DataMember]
        public string Mesaid { get; set; }
        [DataMember]
        public bool Estado { get; set; }
    }
}