﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace ApiWCF.Admin
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Administrador" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Administrador.svc o Administrador.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Administrador : IAdministrador
    {
        public string GetRols()
        {
            List<Negocio.Admin.Rol> ListRol = Negocio.Admin.Rol.GetRols();
            List<Rol> ServiceListRol = new List<Rol>();
            foreach (var Rol in ListRol)
            {
                Rol ServiceRol = new Rol();

                ServiceRol.RolID = Rol.RolID;
                ServiceRol.Descripcion = Rol.Descripcion;

                ServiceListRol.Add(ServiceRol);
            }

            string Json = JsonSerializer.Serialize(ServiceListRol);

            return Json;
        }

        public string Registrar(string u)
        {
            Negocio.Admin.Usuario usuario = JsonSerializer.Deserialize<Negocio.Admin.Usuario>(u);

            Negocio.Admin.RegistroMensaje RM = usuario.Registrar();

            RegistroMensaje RMService = new RegistroMensaje();

            RMService.Estado = RM.Estado;
            RMService.Mensaje = RM.Mensaje;

            string json = JsonSerializer.Serialize(RMService);

            return json;
        }
        
        public string GetAllUsers()
        {
            List<Negocio.Admin.Usuario> ListUsers = Negocio.Admin.Usuario.GetAllUsers();

            List<Usuario> ListUsersService = new List<Usuario>();

            foreach (Negocio.Admin.Usuario u in ListUsers)
            {
                Usuario uService = new Usuario();

                uService.userid = u.userid;
                uService.Rut = u.Rut;
                uService.Nombre = u.Nombre;
                uService.Apellido = u.Apellido;
                uService.Fecha = u.Fecha;
                uService.Email = u.Email;
                uService.roldescripcion = u.roldescripcion;

                ListUsersService.Add(uService);
            }

            string json = JsonSerializer.Serialize(ListUsersService);

            return json;
        }

        public string GetAllMesas()
        {
            List<Negocio.Admin.Mesa> ListMesas = Negocio.Admin.Mesa.GetAllMesas();

            List<Mesa> ListMesasService = new List<Mesa>();

            foreach (Negocio.Admin.Mesa m in ListMesas)
            {
                Mesa mService = new Mesa();

                mService.Mesaid = m.Mesaid;
                mService.Estado = m.Estado;

                ListMesasService.Add(mService);
            }

            string json = JsonSerializer.Serialize(ListMesasService);


            return json;
        }
        public string EliminarUsuario(string id)
        {
            Negocio.Admin.Usuario user = new Negocio.Admin.Usuario();

            user.userid = id;

            Negocio.Clases.Mensaje msg = user.EliminarUsuario();

            RegistroMensaje RM = new RegistroMensaje();

            RM.Estado = msg.Estado;
            RM.Mensaje = msg.Message;

            string json = JsonSerializer.Serialize(RM);

            return json;
        }

        public string AgregarMesa()
        {
            Negocio.Clases.Mensaje msg = Negocio.Admin.Mesa.AgregarMesa();

            RegistroMensaje RM = new RegistroMensaje();

            RM.Estado = msg.Estado;
            RM.Mensaje = msg.Message;

            string json = JsonSerializer.Serialize(RM);

            return json;
        }

        public string EliminarMesa()
        {
            Negocio.Clases.Mensaje msg = Negocio.Admin.Mesa.EliminarMesa();

            RegistroMensaje RM = new RegistroMensaje();

            RM.Estado = msg.Estado;
            RM.Mensaje = msg.Message;

            string json = JsonSerializer.Serialize(RM);

            return json;
        }
    }
}
