﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace ApiWCF.Cocina
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Cocina" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Cocina.svc o Cocina.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Cocina : ICocina
    {
        public string GetAllFalseComandas()
        {
            Negocio.Cocina.Comanda comanda = new Negocio.Cocina.Comanda();

            List<Negocio.Cocina.Comanda> Listcomanda = comanda.GetAllFalseComandas();

            List<Comanda> ListcomandaService = new List<Comanda>();

            foreach (Negocio.Cocina.Comanda c in Listcomanda)
            {
                Comanda comandaService = new Comanda();

                comandaService.ID = c.ID;
                comandaService.Estado = c.Estado;
                comandaService.Hora_Emision = c.Hora_Emision;
                comandaService.MesaID = c.MesaID;
                comandaService.Comentario = c.Comentario;
                comandaService.Platos = new List<Plato>();
                
                foreach (Negocio.Cocina.Plato p in c.Platos)
                {
                    Plato platoService = new Plato();

                    platoService.Cantidad = p.Cantidad;

                    platoService.ID = p.ID;
                    platoService.Nombre_Plato = p.Nombre_Plato;
                    platoService.Descripcion_Plato = p.Descripcion_Plato;
                    platoService.Precio = p.Precio;

                    comandaService.Platos.Add(platoService);

                }

                ListcomandaService.Add(comandaService);
            }

            string json = JsonSerializer.Serialize(ListcomandaService);

            return json;
        }

        public string RegistrarPlato(string platoSTR)
        {
            Negocio.Cocina.Plato plato = JsonSerializer.Deserialize<Negocio.Cocina.Plato>(platoSTR);

            Negocio.Admin.RegistroMensaje RM = plato.RegistrarPlato();

            Message msg = new Message();

            msg.Estado = RM.Estado;
            msg.Mensaje = RM.Mensaje;

            string json = JsonSerializer.Serialize(msg);
            return json;
        }

        public string GetAllPlatos()
        {
            List<Negocio.Cocina.Plato> listPlato = Negocio.Cocina.Plato.GetAllPlatos();
            List<Plato> ListPlatosServicio = new List<Plato>();

            foreach (Negocio.Cocina.Plato p in listPlato)
            {
                Plato PlatoService = new Plato();

                PlatoService.ID = p.ID;
                PlatoService.Nombre_Plato = p.Nombre_Plato;
                PlatoService.Descripcion_Plato = p.Descripcion_Plato;
                PlatoService.Precio = p.Precio;
                PlatoService.Foto = p.Foto;

                ListPlatosServicio.Add(PlatoService);

            }

            string json = JsonSerializer.Serialize(ListPlatosServicio);

            return json;
        }

        public string GetPlatoByID(string platoid)
        {
            Negocio.Cocina.Plato plato = new Negocio.Cocina.Plato();

            plato.ID = platoid;
            plato = plato.GetPlatoByID();

            Plato platoService = new Plato();
            platoService.ID = plato.ID;
            platoService.Nombre_Plato = plato.Nombre_Plato;
            platoService.Descripcion_Plato = plato.Descripcion_Plato;
            platoService.Precio = plato.Precio;
            platoService.Foto = plato.Foto;

            string json = JsonSerializer.Serialize(platoService);

            return json;
        }

        public string EliminarPlatoByID(string platoid)
        {
            Negocio.Cocina.Plato plato = new Negocio.Cocina.Plato();

            plato.ID = platoid;
            Negocio.Admin.RegistroMensaje RM = plato.EliminarPlatoByID();

            Message msg = new Message();

            msg.Estado = RM.Estado;
            msg.Mensaje = RM.Mensaje;

            string json = JsonSerializer.Serialize(msg);

            return json;
        }

        public string EditarPlato(string platoSTR)
        {
            Negocio.Cocina.Plato plato = JsonSerializer.Deserialize<Negocio.Cocina.Plato>(platoSTR);

            Negocio.Admin.RegistroMensaje RM = plato.EditarPlatoByID();

            Message msg = new Message();

            msg.Estado = RM.Estado;
            msg.Mensaje = RM.Mensaje;

            string json = JsonSerializer.Serialize(msg);
            return json;
        }

        public string GetAllIngredientes()
        {
            List<Negocio.Cocina.Ingrediente> ListIngredientes = Negocio.Cocina.Ingrediente.GetAllIngredientes();

            List<Ingrediente> ListIngredientesService = new List<Ingrediente>();

            foreach (Negocio.Cocina.Ingrediente I in ListIngredientes)
            {
                Ingrediente IService = new Ingrediente();

                IService.IngredienteID = I.IngredienteID;
                IService.Descripcion = I.Descripcion;
                IService.Medida = I.Medida;

                ListIngredientesService.Add(IService);
            }

            string json = JsonSerializer.Serialize(ListIngredientesService);

            return json;
        }

        public string AgregarRPI(string RPISTR)
        {
            Negocio.Cocina.RelacionPlatoIngrediente RPI = JsonSerializer.Deserialize<Negocio.Cocina.RelacionPlatoIngrediente>(RPISTR);

            Negocio.Admin.RegistroMensaje RM = RPI.AgregarRPI();

            Message msg = new Message();

            msg.Estado = RM.Estado;
            msg.Mensaje = RM.Mensaje;

            string json = JsonSerializer.Serialize(msg);
            return json;
        }

        public string GetAllIngredienteByPlatoID(string platoID)
        {
            List<Negocio.Cocina.RelacionPlatoIngrediente> ListRPI = Negocio.Cocina.RelacionPlatoIngrediente.GetAllIngredienteByPlatoID(platoID);

            List<RelacionPlatoIngrediente> ListRPIService = new List<RelacionPlatoIngrediente>();

            foreach (Negocio.Cocina.RelacionPlatoIngrediente RPI in ListRPI)
            {
                RelacionPlatoIngrediente RPIService = new RelacionPlatoIngrediente();

                RPIService.IngredienteID = RPI.IngredienteID;
                RPIService.Descripcion = RPI.Descripcion;
                RPIService.Medida = RPI.Medida;
                RPIService.CantidadConsumo = RPI.CantidadConsumo;

                ListRPIService.Add(RPIService);
            }

            string json = JsonSerializer.Serialize(ListRPIService);

            return json;
        }

        public string EliminarRPI(string RPISTR)
        {
            Negocio.Cocina.RelacionPlatoIngrediente RPI = JsonSerializer.Deserialize<Negocio.Cocina.RelacionPlatoIngrediente>(RPISTR);

            Negocio.Admin.RegistroMensaje RM = RPI.EliminarRPI();

            Message msg = new Message();

            msg.Estado = RM.Estado;
            msg.Mensaje = RM.Mensaje;

            string json = JsonSerializer.Serialize(msg);
            return json;
        }


        public string ActualizarComanda(string ComandaID)
        {
            Negocio.Cocina.Comanda comanda = new Negocio.Cocina.Comanda();

            comanda.ID = ComandaID;

            Negocio.Admin.RegistroMensaje RM = comanda.ActualizarComanda();

            string json = JsonSerializer.Serialize(RM);
            return json;
        }

        public bool RegistrarComanda(string ListPlatos, string mesaid, string mensaje, string comandaid)
        {
            Negocio.Cocina.Comanda comanda = new Negocio.Cocina.Comanda();

            List<Negocio.Cocina.Plato> platos = JsonSerializer.Deserialize<List<Negocio.Cocina.Plato>>(ListPlatos);

            bool estado = comanda.RegistrarComanda(mesaid, mensaje, platos,comandaid);

            return estado;
        }

        public string GetIdNewComanda()
        {
            return Negocio.Cocina.Comanda.GetIdNewComanda();
        }

        public bool ComandaCheck(string comandaid)
        {
            return Negocio.Cocina.Comanda.ComandaCheck(comandaid);
        }

        public bool DesocuparMesa(string mesaid)
        {
            return Negocio.Admin.Mesa.DesocuparMesa(mesaid);
        }

        public string GetComandaByID(string ComandaID)
        {
            List<Negocio.Cocina.Plato> listaplatos = Negocio.Cocina.Comanda.GetComandaByID(ComandaID);

            List<Plato> platoservice = new List<Plato>();

            foreach (Negocio.Cocina.Plato p in listaplatos)
            {
                Plato pService = new Plato();

                pService.Cantidad = p.Cantidad;
                pService.ID = p.ID;
                pService.Nombre_Plato = p.Nombre_Plato;
                pService.Descripcion_Plato = p.Descripcion_Plato;
                pService.Precio = p.Precio;

                platoservice.Add(pService);
            }

            string json = JsonSerializer.Serialize(platoservice);
            return json;
        }

        public bool RegistrarReserva(string userid,string fecha, string mesaid, string hora_entrada, string hora_salida)
        {

            string fechaformateada = fecha.Replace('-', '/');


            return Negocio.Cocina.Reserva.CrearReserva(userid, fechaformateada, mesaid,hora_entrada,hora_salida);
        }

        public string GetReservarByUserID(string usuarioid)
        {

            List<Negocio.Cocina.Reserva> ListReserva = Negocio.Cocina.Reserva.GetReservarByUserID(usuarioid);

            List<Reserva> ListService = new List<Reserva>();

            foreach (Negocio.Cocina.Reserva r in ListReserva)
            {
                Reserva rService = new Reserva();

                rService.reservaid = r.reservaid;
                rService.usuarioid = r.usuarioid;
                rService.fecha = r.fecha;
                rService.hora_entrada = r.hora_entrada;
                rService.hora_salida = r.hora_salida;
                rService.mesaid = r.mesaid;
                rService.estado = r.estado;

                ListService.Add(rService);
            }

            string json = JsonSerializer.Serialize(ListService);



            return json;
        }

    }
}
