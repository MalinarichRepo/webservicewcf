﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ApiWCF.Cocina
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "ICocina" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface ICocina
    {
        [OperationContract]
        string GetAllFalseComandas();
        [OperationContract]
        string RegistrarPlato(string platoSTR);
        [OperationContract]
        bool RegistrarComanda(string ListPlatos,string mesaid,string mensaje,string comandaid);
        [OperationContract]
        string GetIdNewComanda();
        [OperationContract]
        bool ComandaCheck(string comandaid);
        [OperationContract]
        bool DesocuparMesa(string mesaid);
        [OperationContract]
        string GetAllPlatos();
        [OperationContract]
        string GetPlatoByID(string platoid);
        [OperationContract]
        string EliminarPlatoByID(string platoid);
        [OperationContract]
        string EditarPlato(string platoSTR);

        [OperationContract]
        string GetAllIngredientes();
        [OperationContract]
        string AgregarRPI(string RPISTR);
        [OperationContract]
        string GetAllIngredienteByPlatoID(string platoID);
        [OperationContract]
        string EliminarRPI(string RPISTR);
        [OperationContract]
        string ActualizarComanda(string ComandaID);
        [OperationContract]
        string GetComandaByID(string ComandaID);
        [OperationContract]
        bool RegistrarReserva(string userid,string fecha, string mesaid, string hora_entrada, string hora_salida);
        [OperationContract]
        string GetReservarByUserID(string usuarioid);

    }

    [DataContract]
    public class Reserva
    {
        [DataMember]
        public string reservaid { get; set; }
        [DataMember]
        public string usuarioid { get; set; }
        [DataMember]
        public string fecha { get; set; }
        [DataMember]
        public string hora_entrada { get; set; }
        [DataMember]
        public string hora_salida { get; set; }
        [DataMember]
        public string mesaid { get; set; }
        [DataMember]
        public bool estado { get; set; }
    }
    [DataContract]
    public class Ingrediente
    {
        [DataMember]
        public string IngredienteID { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public string Medida { get; set; }


    }

    [DataContract]
    public class RelacionPlatoIngrediente
    {
        [DataMember]
        public string PlatoID { get; set; }
        [DataMember]
        public string IngredienteID { get; set; }
        [DataMember]
        public string CantidadConsumo { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public string Medida { get; set; }
    }
    [DataContract]
    public class Comanda
    {
        [DataMember]
        public string ID { get; set; }
        [DataMember]
        public string Hora_Emision { get; set; }
        [DataMember]
        public int Estado { get; set; }
        [DataMember]
        public string MesaID { get; set; }
        [DataMember]
        public string Comentario { get; set; }
        [DataMember]
        public List<Plato> Platos { get; set; }
    }
    [DataContract]
    public class Plato
    {
        [DataMember]
        public string ID { get; set; }
        [DataMember]
        public string Nombre_Plato { get; set; }
        [DataMember]
        public string Descripcion_Plato { get; set; }
        [DataMember]
        public int Precio { get; set; }
        [DataMember]
        public byte[] Foto { get; set; }
        [DataMember]
        public int Cantidad { get; set; }
    }

    [DataContract]
    public class Message
    {
        [DataMember]
        public bool Estado { get; set; }
        [DataMember]
        public string Mensaje { get; set; }

    }
}
