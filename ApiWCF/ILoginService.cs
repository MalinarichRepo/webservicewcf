﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ApiWCF
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "ILoginService" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface ILoginService
    {
        [OperationContract]
        bool ConnectionStatus();
        [OperationContract]
        string Login(string user);
        [OperationContract]
        string GetPerfil(string userid);
        [OperationContract]
        string ResetPassword(string email);
        [OperationContract]
        string ChangePassword(string userid,string newpassword);
        [OperationContract]
        string EditUser(string user);


    }
    [DataContract]
    public class LoginRespuesta
    {
        [DataMember]
        public bool Estado { get; set; }
        [DataMember]
        public string Mensaje { get; set; }
        [DataMember]
        public string RolID { get; set; }
        [DataMember]
        public string UserID { get; set; }
    }
    [DataContract]
    public class Usuario
    {
        [DataMember]
        public string Rut { get; set; }
        [DataMember]
        public string Nombre { get; set; }
        [DataMember]
        public string Apellido { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string roldescripcion { get; set; }
        [DataMember]
        public string Fecha { get; set; }
    }
}