﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ApiWCF.Finanzas
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IFinanzas" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IFinanzas
    {
        [OperationContract]
        string GetAllRecordDiarios();

        [OperationContract]
        string RegistrarProveedor(string ProveedorSTR);
        [OperationContract]
        string GetAllProveedores();
        [OperationContract]
        bool RegistrarFactura(string FacturaSTR);
        [OperationContract]
        string GetAllFacturas();
        [OperationContract]
        bool RegistrarProducto(string ProductoSTR);
        [OperationContract]
        string GetAllMedidas();
        [OperationContract]
        bool ActualizarFactura(string idfactura);
        [OperationContract]
        string GetAllDetallesByFacturaID(string idFactura);




    }
    [DataContract]
    class DetalleFactura
    {
        [DataMember]
        public string Idproducto { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public string Medida_desc { get; set; }
        [DataMember]
        public string Cantidad { get; set; }
    }

        [DataContract]
    class Medida
    {
        [DataMember]
        public string Medidaid { get; set; }
        [DataMember]
        public string Medida_desc { get; set; }
    }
        [DataContract]
    class Factura
    {
        [DataMember]
        public string ID { get; set; }
        [DataMember]
        public string Fecha_Emision { get; set; }
        [DataMember]
        public string Tipo_Mercaderia { get; set; }
        [DataMember]
        public string Valor_Neto { get; set; }
        [DataMember]
        public string Valor_Bruto { get; set; }
        [DataMember]
        public string Rut_Proveedor { get; set; }
        [DataMember]
        public bool Estado { get; set; }
    }
        [DataContract]
    class RegistroMensaje
    {
        [DataMember]
        public bool Estado { get; set; }
        [DataMember]
        public string Mensaje { get; set; }
    }

    [DataContract]
    class RecordUtilDiaria
    {
        [DataMember]
        public string ID { get; set; }
        [DataMember]
        public DateTime Fecha { get; set; }
        [DataMember]
        public decimal Gasto { get; set; }
        [DataMember]
        public decimal Ingreso { get; set; }
        [DataMember]
        public decimal Utilidad { get; set; }
        [DataMember]
        public decimal Clientes_Atendido { get; set; }
        [DataMember]
        public decimal Platos_Consumidos { get; set; }
        [DataMember]
        public decimal Media_Tiempo_Atencion_HR { get; set; }
    }
    [DataContract]
    class Proveedor
    {
        [DataMember]
        public string Rut { get; set; }
        [DataMember]
        public string Nombre { get; set; }
        [DataMember]
        public string Telefono1 { get; set; }
        [DataMember]
        public string Telefono2 { get; set; }
        [DataMember]
        public string Direccion { get; set; }


    }
}
