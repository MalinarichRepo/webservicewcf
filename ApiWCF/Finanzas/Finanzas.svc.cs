﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace ApiWCF.Finanzas
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Finanzas" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Finanzas.svc o Finanzas.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Finanzas : IFinanzas
    {
        public string GetAllRecordDiarios()
        {
            List<Negocio.Finanzas.RecordUtilDiaria> ListRecords = Negocio.Finanzas.RecordUtilDiaria.GetAllRecords();

            List<RecordUtilDiaria> newListRecords = new List<RecordUtilDiaria>();

            foreach (Negocio.Finanzas.RecordUtilDiaria r in ListRecords)
            {
                RecordUtilDiaria rud = new RecordUtilDiaria();

                rud.ID = r.ID;
                rud.Fecha = r.Fecha;
                rud.Gasto = r.Gasto;
                rud.Ingreso = r.Ingreso;
                rud.Utilidad = r.Utilidad;
                rud.Clientes_Atendido = r.Clientes_Atendido;
                rud.Platos_Consumidos = r.Platos_Consumidos;
                rud.Media_Tiempo_Atencion_HR = r.Media_Tiempo_Atencion_HR;

                newListRecords.Add(rud);
            }

            string json = JsonSerializer.Serialize(newListRecords);

            return json;
        }

        public string RegistrarProveedor(string ProveedorSTR)
        {
            Negocio.Finanzas.Proveedor proveedor = new Negocio.Finanzas.Proveedor();

            proveedor = JsonSerializer.Deserialize<Negocio.Finanzas.Proveedor>(ProveedorSTR);

            Negocio.Admin.RegistroMensaje RM = proveedor.RegistrarProveedor();

            RegistroMensaje RMService = new RegistroMensaje();

            RMService.Estado = RM.Estado;
            RMService.Mensaje = RM.Mensaje;

            string json = JsonSerializer.Serialize(RMService);
            return json;
        }

        public string GetAllProveedores()
        {

            List<Negocio.Finanzas.Proveedor> ListProveedores = Negocio.Finanzas.Proveedor.GetAllProveedores();

            List<Proveedor> ListProveedoresService = new List<Proveedor>();

            foreach (Negocio.Finanzas.Proveedor p in ListProveedores)
            {
                Proveedor pService = new Proveedor();

                pService.Rut = p.Rut;
                pService.Nombre = p.Nombre;
                pService.Telefono1 = p.Telefono1;
                pService.Telefono2 = p.Telefono2;
                pService.Direccion = p.Direccion;

                ListProveedoresService.Add(pService);
            }

            string json = JsonSerializer.Serialize(ListProveedoresService);

            return json;
        }

        public bool RegistrarFactura(string FacturaSTR)
        {
            Negocio.Finanzas.Factura factura = JsonSerializer.Deserialize<Negocio.Finanzas.Factura>(FacturaSTR);
            return factura.RegistrarFactura();
        }

        public string GetAllFacturas()
        {
            List<Negocio.Finanzas.Factura> ListFacturas = Negocio.Finanzas.Factura.GetAllFacturas();
            List<Factura> ListService = new List<Factura>();

            foreach (Negocio.Finanzas.Factura F in ListFacturas)
            {
                Factura fService = new Factura();

                fService.ID = F.ID;
                fService.Fecha_Emision = F.Fecha_Emision;
                fService.Tipo_Mercaderia = F.Tipo_Mercaderia;
                fService.Valor_Neto = F.Valor_Neto;
                fService.Valor_Bruto = F.Valor_Bruto;
                fService.Rut_Proveedor = F.Rut_Proveedor;
                fService.Estado = F.Estado;

                ListService.Add(fService);
            }

            string json = JsonSerializer.Serialize(ListService);

            return json;
        }

        public string GetAllMedidas()
        {
            List<Negocio.Finanzas.Medida> ListMedida = Negocio.Finanzas.Medida.GetAllMedidas();
            List<Medida> ListService = new List<Medida>();

            foreach (Negocio.Finanzas.Medida M in ListMedida)
            {
                Medida mService = new Medida();

                mService.Medidaid = M.Medidaid;
                mService.Medida_desc = M.Medida_desc;

                ListService.Add(mService);
            }

            string json = JsonSerializer.Serialize(ListService);
            return json;
        }

        public bool RegistrarProducto(string ProductoSTR)
        {
            Negocio.Finanzas.Producto factura = JsonSerializer.Deserialize<Negocio.Finanzas.Producto>(ProductoSTR);
            return factura.RegistrarProducto();
        }

        public bool ActualizarFactura(string idfactura)
        {
            Negocio.Finanzas.Factura factura = new Negocio.Finanzas.Factura();

            factura.ID = idfactura;

            bool estado = factura.ActualizarFactura();
            return estado;
        }

        public string GetAllDetallesByFacturaID(string idFactura)
        {
            List<Negocio.Finanzas.DetalleFactura> ListDF = Negocio.Finanzas.DetalleFactura.GetDetallesByIDFactura(idFactura);
            List<DetalleFactura> ListService = new List<DetalleFactura>();
            foreach (Negocio.Finanzas.DetalleFactura DF in ListDF)
            {
                DetalleFactura dfService = new DetalleFactura();

                dfService.Idproducto = DF.Idproducto;
                dfService.Descripcion = DF.Descripcion;
                dfService.Medida_desc = DF.Medida_desc;
                dfService.Cantidad = DF.Cantidad;

                ListService.Add(dfService);
            }

            string json = JsonSerializer.Serialize(ListService);

            return json;
        }
    }
}
