﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Negocio.Login;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace ApiWCF
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "LoginService" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione LoginService.svc o LoginService.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class LoginService : ILoginService
    {
        public bool ConnectionStatus()
        {
            ConnectionStatus conn = new ConnectionStatus();
            bool Status = conn.OpenConnection();

            return Status;
        }

        public string GetPerfil(string userid)
        {
            Negocio.Admin.Usuario user = new Negocio.Admin.Usuario();

            user.userid = userid;

            user = user.GetPerfil();

            Usuario UserService = new Usuario();

            UserService.Rut = user.Rut;
            UserService.Nombre = user.Nombre;
            UserService.Apellido = user.Apellido;
            UserService.Email = user.Email;
            UserService.roldescripcion = user.roldescripcion;
            UserService.Fecha = user.Fecha;

            string json = JsonSerializer.Serialize(UserService);

            return json;
        }

        public string Login(string user)
        {
            Negocio.Admin.Usuario Usuario = JsonSerializer.Deserialize<Negocio.Admin.Usuario>(user);

            Negocio.Clases.LoginRespuesta respuesta = Negocio.Login.Login.Loguear(Usuario);

            LoginRespuesta re = new LoginRespuesta();

            re.Estado = respuesta.Estado;
            re.Mensaje = respuesta.Mensaje;
            re.RolID = respuesta.RolID;
            re.UserID = respuesta.UserID;

            string json = JsonSerializer.Serialize(re);

            return json;
        }

        
        public string ResetPassword(string email)
        {
            Negocio.Admin.Usuario User = new Negocio.Admin.Usuario();

            User.Email = email;

            Negocio.Clases.Mensaje msg =  User.ResetPassword();

            LoginRespuesta msgService = new LoginRespuesta();

            msgService.Estado = msg.Estado;
            msgService.Mensaje = msg.Message;

            string json = JsonSerializer.Serialize(msgService);

            return json;
        }

        public string ChangePassword(string userid, string newpassword){

            Negocio.Admin.Usuario user = new Negocio.Admin.Usuario();

            user.userid = userid;

            user = user.GetPerfil();

            Negocio.Clases.Mensaje msg = user.ChangePassword(newpassword);

            LoginRespuesta msgService = new LoginRespuesta();

            msgService.Estado = msg.Estado;
            msgService.Mensaje = msg.Message;

            string json = JsonSerializer.Serialize(msgService);

            return json;
        }
        public string EditUser(string user)
        {
            Negocio.Admin.Usuario usuario = JsonSerializer.Deserialize<Negocio.Admin.Usuario>(user);

            Negocio.Clases.Mensaje msg = usuario.EditUser();

            LoginRespuesta msgService = new LoginRespuesta();

            msgService.Estado = msg.Estado;
            msgService.Mensaje = msg.Message;

            string json = JsonSerializer.Serialize(msgService);
            return json;
        }
    }
}
