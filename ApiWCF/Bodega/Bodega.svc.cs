﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace ApiWCF.Bodega
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Bodega" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Bodega.svc o Bodega.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Bodega : IBodega
    {
        public string GetAllStock()
        {
            List<Negocio.Bodega.Stock> ListStock = Negocio.Bodega.Stock.GetAllStock();

            List<Stock> ListStockService = new List<Stock>();


            foreach (Negocio.Bodega.Stock s in ListStock)
            {
                Stock StockService = new Stock();

                StockService.id = s.id;
                StockService.descripcion = s.descripcion;
                StockService.cantidad = s.cantidad;
                StockService.medida = s.medida;

                ListStockService.Add(StockService);
            }

            string json = JsonSerializer.Serialize(ListStockService);

            return json;
        }

        public bool PedirInsumos(string mensaje)
        {
            Negocio.Bodega.Stock stock = new Negocio.Bodega.Stock();
            bool respuesta = stock.PedirInsumo(mensaje);
            return respuesta;
        }

    }

    
}
