﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ApiWCF.Bodega
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IBodega" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IBodega
    {
        [OperationContract]
        string GetAllStock();
        [OperationContract]
        bool PedirInsumos(string mensaje);
    }

    [DataContract]
    public class Stock
    {
        [DataMember]
        public string id { get; set; }
        [DataMember]
        public string descripcion { get; set; }
        [DataMember]
        public int cantidad { get; set; }
        [DataMember]
        public string medida { get; set; }
    }
}
